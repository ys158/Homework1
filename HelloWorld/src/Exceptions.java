
public class Exceptions {

	public static void main(String[] args) {
		Integer invalidInput1 = 0;
		Integer invalidInput2 = Integer.MAX_VALUE;
		Integer validInput = 1;
		
		devidingCheck(invalidInput1);
		System.out.println("-----------------");
		
		devidingCheck(validInput);
		System.out.println("-----------------");
		
		overFlowCheck(invalidInput2);
		System.out.println("-----------------");
		
		try{
			throwException(invalidInput1);
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println("Exception detected!Dealing with it!");
			System.out.println(10/(invalidInput1+1));
		}
		System.out.println("-----------------");
		
		try{
			throwException(validInput);
		}
		catch(Exception e){
			System.err.println(e.getMessage());
			System.out.println("Exception detected!Dealing with it!");
			System.out.println(10/(invalidInput1+1));
		}
		System.out.println("-----------------");
	}

	private static void devidingCheck(int x) {
		try{
			System.out.println(10/x);
		}
		catch(ArithmeticException e){
			System.out.println("The devisor can not be 0!Dealing with it!");
			x+=1;
			System.out.println(10/x);	}
	}

	private static void overFlowCheck(int x){
		try
        {
			int y = 1;
			int z = Math.addExact(x,y);
            System.out.println(x + " + " + y + " = " + z);
        }
        catch (ArithmeticException e)
        {
            System.out.println("Integer overflow detected!Now dealing with it!");
            long z = (long) x + (long) 1;
            System.out.println(x + " + " + 1 + " = " + z);
        }
	}
	
	private static void throwException(int x) throws ArithmeticException{
		if(x==0){
			throw new ArithmeticException("We cannot devide by zero!");
		}
		System.out.println(10/x);
	}

}
