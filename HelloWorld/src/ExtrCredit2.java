//solving the problem of palindrome number!
public class ExtrCredit2 {

	public static void main(String[] args) {
		int[] test = new int[4];
		test[0] = 101;
		test[1] = 23344332;
		test[2] = 12231;
		test[3] = 0;
		for(int item : test){
			System.out.println(isPalindrome(item));
		}
	}
	private static boolean isPalindrome(int n) {
	    if( n < 0){
	      return false;
	    }
	    int x = 0;
	    int tmp = n;
	    while(tmp != 0){
	        x = tmp % 10 + x * 10;
	        tmp /= 10;
	    }
	    return x == n;
	}
}
