//solving the problem of calculating the power of 3!
public class ExtraCredit {

	public static void main(String[] args) {
		boolean[] boo = new boolean[3];
		boo[0] = isPowerOfThree(81);
		boo[1] = isPowerOfThree(10);
		boo[2] = isPowerOfThree(19827);
		for(boolean item : boo){
			System.out.println(item);
		}
	}
	
	private static boolean isPowerOfThree(int x) {
		return (Math.log10(x) / Math.log10(3)) % 1 == 0;
		}
}
