import robots.robot;
import robots.new_guy;
import robots.old_guy;
import robots.cracker;

public class Object_and_loop {

	public static void main(String[] args) {
		robot BB_8 = new new_guy("Genius-200");
		robot Eniac = new old_guy("Past-100");
		robot crap = new cracker("Jeez");
		robot[] Robots = new robot[3];
		Robots[0] = BB_8;
		Robots[1] = Eniac;
		Robots[2] = crap;
		for(robot i : Robots){
			System.out.println("This is the type \""+ i.getType() + "\" of the Robots type array!");
			i.makeMovement();
			System.out.println("------------------------");
		}
		new_guy BB_9 = new new_guy("Genius-300");
		BB_9.interact();
		System.out.println("------------------------");
	}

}
