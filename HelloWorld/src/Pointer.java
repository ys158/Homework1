import robots.new_guy;

public class Pointer {

	public static void main(String[] args) {
		passByValue();
		System.out.println("-----------------------------");
		passReferenceByValue();
		System.out.println("-----------------------------");
		twoString();
		System.out.println("-----------------------------");
	}
	
	private static void passByValue() {
		new_guy BB_8 = new new_guy("Genius-200");
		ref(BB_8);
	
		if (BB_8.getType().equals("Genius-200")) {//true
			System.out.println( "Name cannot be changed through func ref(),since it reassigned the reference!" );
		} else if (BB_8.getType().equals("Genius-100")) {
			System.out.println( "Type has been changed through func ref()" );
		}
	}
	
	private static void ref(new_guy x){
		x = new new_guy("Genius-100");
	}
	
	private static void passReferenceByValue(){
		new_guy BB_8 = new new_guy("Genius-200");
		testReference(BB_8);
		if (BB_8.getType().equals("Genius-200")) {
			System.out.println( "Java passes copy of value as the value!" );

		} else if (BB_8.getType().equals("Genius-100")) {//true
			System.out.println( "Java passes copy of reference as the value!"
					+ "The value at where the reference is point to can be changed through reference!" );
		}
	}
	
	private static void testReference(new_guy type){
		type.chType("Genius-100");
	}
	
	private static void twoString(){
		String s1 = "No.1";
		String s2 = "No.1";
		String s3 = new String("No.2");
		String s4 = new String("No.2");
		if(s1 == s2){
			System.out.println("Strings having the same value are equal!");
		}
		else{
			System.out.println("Strings having the same value are not equal!");
		}
		System.out.println("-----------------------------");
		if(s3 == s4){
			System.out.println("String objects can be compared by == ");
        }
		
		if(s3.equals(s4)){
			System.out.println("String objects can be compared by .equals() func!");
		}
	}
	

}
