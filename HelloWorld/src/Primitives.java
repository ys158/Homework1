
public class Primitives {

	public static void main(String[] args) {
		int x = 2; //32-bit signed-int
		boolean bool = false; //true or false
		char c = '@'; //single character
		double d = 3.14; //64-bit floating point
		byte b = 8; //8-bit signed
		float f = 5.5243f; //32-bit floating point
		long l = 500L; //64-bit signed
		short s = 5; //16-bit signed	
		System.out.println("The most commonly used primitives are int, boolean, char, \nand double, and long, short are used less frequently.\nBy the way,Java also includes objects that wrap primitives.\nThe Object is the full name of the primitive using a capital letter.");
		Integer y = 17;
		Boolean boo = true;
		Character ch = 's';
	}

}
