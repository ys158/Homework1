
public class Print {
	public static void main(String[] args){
		simplePrint();
		System.out.println("------------------------");
		concatPrint();
		System.out.println("------------------------");
		newlinePrint();
		System.out.println("------------------------");
		complexPrint();
		System.out.println("------------------------");
	}
	
	private static void simplePrint(){
		System.out.println("My name is Yiling Shi!");
	}
	
	private static void concatPrint(){
		int x = 23;
		System.out.println("I am " + x + " years old now!");
	}
	
	private static void newlinePrint(){
		System.out.println("There is a newline character after me \n Yeah");
		System.out.println();
		System.out.println("Above is a new line by printing a blank line!");
	}
	
	private static void complexPrint(){
		int num = 1234;
		String s = "+ This is a Java string";
		System.out.println("We can print" + " " + num + " " + s + ", a bunch of staff,but special cases are: \\,\',\"");
	}
}
