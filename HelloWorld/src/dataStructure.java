import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class dataStructure {

	public static void main(String[] args) {
		arrayTest();
		System.out.println();
		listTest();
		System.out.println();
		setTest();
		System.out.println();
		mapTest();
	}
	
	private static void arrayTest() {
		int[] array = new int[5];
		array[0] = 1;
		array[1] = 3;
		array[2] = 5;
		array[3] = 7;
		array[4] = 9;
		for(int item : array){
			System.out.println(item+"\n----------------");
		}
	}
		
		private static void listTest() {
			List<Integer> list = new ArrayList<>();
			list.add(Integer.MIN_VALUE);
			list.add(0);
			list.add(1);
			list.add(2);
			list.add(Integer.MAX_VALUE);
			for(int item : list){
				System.out.println(item + "\n---------------");
			}
		}
		
		private static void setTest() {
			Set<String> set = new HashSet<>();//unordered hashset
			set.add("first");
			set.add("second");
			set.add("third");
			for(String item : set){
				System.out.println(item + "\n--------------");
			}	
		}
		
		private static void mapTest(){
			Map<String,String> map = new HashMap<>();//unordered hashmap
			map.put("first", "John");
			map.put("second", "Davis");
			map.put("third", "Moore");
			for(String item : map.keySet()){
				System.out.println(item + " : " + map.get(item) + "\n--------------");
			}
		}

}
