package robots;

public class new_guy extends robot implements Interact{

	public new_guy(String type) {
		super(type);
	}

	@Override
	public void makeMovement(){
		System.out.println("I can move my body at any direction!");
	}
	
	@Override
	public void interact(){
		System.out.println("I can respond to your words,my lord!");
	}
	
}
