package robots;

public abstract class robot {
	private String type;
	
	public robot(String type){
		this.type = type;
	}
	
	public String getType(){
		return this.type;
	}
	
	public void chType(String type){
		this.type = type;
	}
	
	public abstract void makeMovement();

}
