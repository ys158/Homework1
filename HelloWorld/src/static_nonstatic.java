
public class static_nonstatic {
	public static final String Java = "Java class";
			public static void main(String[] args) {	
				System.out.println(Java);	
				//for static method in Java,it can be called without creating an object
				System.out.println(funcStatic());
				//for non-static method in Java,an object needs to be created first.
				static_nonstatic non = new static_nonstatic();
				System.out.println(non.funcNonStatic());
			}
			//static function belongs to the class ,not other object.
			private static String funcStatic(){
				return Java;
			}
			
			private String funcNonStatic(){
				return Java;
			}
	}


